import React from 'react';
import GoogleMapReact from 'google-map-react';
import { MdRoom } from "react-icons/md";


const MapPin=()=>{
    return(
        <MdRoom style={{wifth:25, height:25}}/>
    );
}


const GoogleMapComponent = (props) =>{
    return (
        <div md={10} style={{height:'100vh', width:'85%', margin:50}}>
            <GoogleMapReact
                bootstrapURLKeys={{key:`${process.env.REACT_APP_GOOGLE_CLIENT_ID}`}}
                defaultCenter={{lat:45.508888,lng: -73.561668}}
                defaultZoom={13}>
                
                {Array.isArray(props.data) && props.data.map((record) => record.latitude && record.longitude ? (
                    <MapPin
                        lat={record.latitude}
                        lng={record.longitude}
                    />
                    ):null
                )}
            </GoogleMapReact>
        </div>
    );
};

export default GoogleMapComponent;