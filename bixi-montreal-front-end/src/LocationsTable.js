import { Grid, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    table:{
        maxHeight:500,
        margin:50
    }
});
const LocationsTable = (props) =>{
    const classes = useStyles();
    return (
        <Grid md={10}>
            <TableContainer classes={{root:classes.table}} component={Paper}>
                <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" onClick={()=>props.headerClick('Code')}>Code</TableCell>
                            <TableCell align="center" onClick={()=>props.headerClick('name')}>Name</TableCell>
                            <TableCell align="center">Latitude</TableCell>
                            <TableCell align="center">Longitude</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.data.map((row) => (
                            <TableRow key={row.Code}>
                                <TableCell align="center">{row.Code}</TableCell>
                                <TableCell align="center" width={250}>{row.name}</TableCell>
                                <TableCell align="center">{row.latitude}</TableCell>
                                <TableCell align="center">{row.longitude}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>
    );
}

export default LocationsTable;