import React, { useEffect, useState } from 'react';
import './App.css';
import Axios from 'axios';
import { Container } from '@material-ui/core';
import LocationsTable from './LocationsTable';
import GoogleMapComponent from './GoogleMapComponent';

function App() {
  const [gridData, setGridData] = useState([]);

  async function fetchGridData(headerKey){
    await Axios.get(`${process.env.REACT_APP_GRID_DATA}${headerKey}`).then((res)=>{

      setGridData(res.data.grid_data)
    });
  }

  useEffect(()=>{
    fetchGridData(null)
  },[])

  return (
    <Container>
      <LocationsTable data={gridData} headerClick={fetchGridData} />
      <GoogleMapComponent data={gridData} />
    </Container>
  );
}

export default App;
