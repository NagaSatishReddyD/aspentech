from flask import Flask, make_response
from flask_cors import CORS
from os import path


app=Flask(__name__)
CORS(app)
app.config.update(
    FILE_NAME='../biximontrealrentals2019-33ea73/Stations_2019.csv'
)


@app.route("/<sort_key>")
def fetch_file_data(sort_key):
    if not path.exists(app.config.get('FILE_NAME')):
        return make_response('File Not Found', 400)
    

    with open(app.config.get('FILE_NAME'), 'r', encoding='utf-8') as file:
        file_data = file.read().strip()
        if len(file_data) == 0:
            return make_response('No data exist in the file', 200)
    
    file_lines = file_data.split('\n')
    grid_data = [(file_lines[0].split(","))]
    for i in range(1, len(file_lines)):
        data = file_lines[i].split(",")
        data[0] = int(data[0])
        grid_data.append(data)

    # header_line
    header = grid_data[0]
    grid_content = grid_data[1:]

    if sort_key != 'null':
        sort_key_index = header.index(sort_key)
        grid_content = sorted(grid_content, key=lambda x: x[sort_key_index])

    grid_data = []
    for data in grid_content:
        grid_data.append(dict(zip(header, data)))

    return make_response({'grid_data': grid_data}, 200)