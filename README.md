The project is developed using below technologies
Front End(bixi-montreal-front-end):  React

Back-End(bixi-montreal-rentals-back-end): Python, Flask Framework

HOW TO RUN

Front End:
Note: Make sure you add the Google Client Id in the .env file to display the google map component
Make sure NodeJs is installed in the system
Open command prompt. Go to bixi-montreal-front-end folder

1. npm i
2. npm start

Backend:
Make sure python is installed in the system.
Open command prompt. Go to bixi-montreal-rentals-back-end folder

When the backend is running for the first time. Run the below commands

1. py -m venv env
2. \env\Scripts\activate
3. pip install Flask
4. pip install flask_cors

Run the below commands to run the app
1. \env\Scripts\activate
2. SET FLASK_APP=main.py
3. flask run


Once the UI and Backend started running both the grid and map components will be loaded at the URL http://localhost:3000/

[The output is in the output.png](https://gitlab.com/NagaSatishReddyD/aspentech/-/blob/master/output.PNG)
